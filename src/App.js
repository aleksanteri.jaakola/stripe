import React from "react";
import Navbar from "./Navbar";
import Hero from "./Hero";
import Sidebar from "./Sidebar";
import Submenu from "./Submenu";
//import Darkmode from "./darkmode"; needs to be fixed
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/">
            <Navbar />
            <Sidebar />
            <Submenu />
            <Hero />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
