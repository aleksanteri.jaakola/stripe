import React, { useState, useEffect } from "react";

function Darkmode() {
  const [theme, setTheme] = useState("light-theme");

  const toggleTheme = () => {
    if (theme === "light-theme") {
      setTheme("dark-theme");
    } else {
      setTheme("light-theme");
    }
  };
  useEffect(() => {
    document.documentElement.className = theme;
  }, [theme]); 
  return (
    <>
      <nav>
        <div>
          <button className="darkmode-btn" onClick={toggleTheme}>
            toggle
          </button>
        </div>
      </nav>
    </>
  );
}
export default Darkmode;
